// base imports
import React, { Fragment, useState, useContext } from 'react';

// { Component as Alias }
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Switch } from 'react-router-dom';

// bootstrap
import { Container } from 'react-bootstrap';

// app components
import Navbar from './components/Navbar';

// page components
import Home from './pages/Home';
import Courses from './pages/Courses';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import NotFound from './pages/NotFound';
import CreateCourse from './pages/CreateCourse';
import EditCourse from './pages/EditCourse';
import DeleteCourse from'./pages/DeleteCourse';


import { UserProvider } from './UserContext';

export default function App() {

	// State hook for the user state thats defined here for a global scope
	// Initialized as an object with properties from the local storage
	const [user, setUser] = useState({
		email: localStorage.getItem('email'),
		
		// Data stored in localStorage is converted into string. Added condition 
		isAdmin: localStorage.getItem('isAdmin') === 'true'
	})

	// Function for clearing localstorage on logout
	const unsetUser = () => {
		localStorage.clear();

		// changes the value of  the user state back to it's original value
		setUser({
			email: null,
			isAdmin: null
		})
	}

	// /////////////////////////////////DeleteCourse//////////////////////////////////
	// const [course, setCourse] = useState({
	// 	onOffer: localStorage.getItem('onOffer')
	// })

	// const unsetOffer = () => {
	// 	setCourse({
	// 		onOffer: false
	// 	})
	// }
	
  return (
    <Fragment>
    	<UserProvider value={{user, setUser, unsetUser}}>
	    	<Router>
			    <Navbar />
			      	<Container className="my-5">
			      		<Switch>
							<Route exact path="/" component={Home}/>
							<Route exact path="/courses" component={Courses}/>
							<Route exact path="/logout" component={Logout}/>
							<Route exact path="/login" component={Login}/>
							<Route exact path="/register" component={Register}/>
							<Route exact path="/createcourse" component={CreateCourse}/>
							<Route exact path="/editcourse" component={EditCourse}/>
							<Route exact path="/deletecourse" component={DeleteCourse}/>
							<Route component = {NotFound} />
						</Switch>
			      	</Container>
		     </Router>
	     </UserProvider>
    </Fragment>
  );
}

