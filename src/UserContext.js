// base imports
import React from 'react';

// Create a context object
const UserContext = React.createContext();

export const UserProvider = UserContext.Provider;

export default UserContext;

// context object
// UserContext {
// 	user: ""
// 	setUser: () => {}
// 	unsetUser: () => {}
// 	Provider: () => {}
// }