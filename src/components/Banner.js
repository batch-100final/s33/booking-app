// base imports
import React from 'react';

// bootstrap
import {Jumbotron, Button, Row, Col} from 'react-bootstrap';

// export default allows the function to be used in other files
export default function Banner(){
	return(
		<Row>
			<Col>
				<Jumbotron>
					<h1>Jumbotron Title</h1>
					<p> Banner description </p>
					<Button variant="primary">Enroll Now</Button>
				</Jumbotron>
			</Col>
		</Row>
	)
}