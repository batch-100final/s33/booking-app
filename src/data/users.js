const userData = [
	{
		email: "admin@gmail.com",
		password: "admin",
		isAdmin: true
	},
	{
		email: "user@gmail.com",
		password: "user",
		isAdmin: false
	}
]

export default userData;