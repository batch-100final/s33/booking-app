// base imports
import React, {Fragment, useContext} from 'react';
import { Link } from 'react-router-dom';

// bootstrap
import { Table, Button } from 'react-bootstrap';

// app components
import Course from '../components/Course';

// app data
import coursesData from '../data/courses';

//
import UserContext from '../UserContext';

export default function Courses(){
	// use the UserContext and destructure it to access the user state defined in app component
	const { user } = useContext(UserContext);

	// table rows to be rendered in a bootstrap table when an admin is logged in
	const courseRows = coursesData.map((course) => {
		// console.log(course.onOffer)
		// delete course
		
		function disable() {
			course.onOffer = false;
			 alert('Course successfully deleted');
			console.log(course.onOffer);
		}

		function activate() {
			course.onOffer = true;
			alert('Course has been activated');
			console.log(course.onOffer);
		}

		return (
			<tr key={course.id}>
				<td>{course.id}</td>
				<td>{course.name}</td>
				<td>{course.description}</td>
				<td>{course.price}</td>
				<td>{course.onOffer ? 'open' : 'closed'}</td>
				<td>{course.start_date}</td>
				<td>{course.end_date}</td>
				<td>
					<Link to="/EditCourse"> <Button variant="warning">Update</Button> </Link>
					<Button variant="danger" onClick = {disable}>Disable</Button>
					<Button variant="info" onClick = {activate}>Activate</Button>
				</td>
			</tr>
		)
	})

	const courseCards = coursesData.map((course) => {
		if(course.onOffer) {
			return (
				<Course key = {course.id} course = {course} />
			)
		} else {
			return null;
		}
		
	})


	return(
		user.isAdmin === true 
		?
		<Fragment>
			<h1>Courses Dashboard</h1>
			<Table>
				<thead>
					<tr>
						<th>Course ID</th>
						<th>Name</th>
						<th>Description</th>
						<th>Price</th>
						<th>On Offer</th>
						<th>Start Date</th>
						<th>End Date</th>
					</tr>
				</thead>
				<tbody>
					{courseRows}
				</tbody>
			</Table>
		</Fragment>
		:
		<Fragment>
			{courseCards}
		</Fragment>
	)
}

