// base imports
// import React, { Fragment, useContext } from 'react';
import React, { Fragment } from 'react';


// bootstrap 
import { Form, Button } from 'react-bootstrap';

// app data
// import coursesData from '../data/courses';

//
// import UserContext from '../UserContext';

export default function CreateCourse() {
	// const { user } = useContext(UserContext);

	return(
		<Fragment>
			<h3>Edit Course</h3>
            <Form>
                <Form.Group>
                    <Form.Label>Course Name</Form.Label>
                    <Form.Control 
                        type="text" 
                        placeholder="Enter Course Name" 
                        required
                    />
                </Form.Group>
               
               <Form.Group>
                    <Form.Label>Course Description</Form.Label>
                    <Form.Control 
                        as="textarea" 
                        rows={3}
                        placeholder="Enter Course Description" 
                        required
                    />
                </Form.Group>

                <Form.Group>
                    <Form.Label>Course Price</Form.Label>
                    <Form.Control 
                        type="number" 
                        placeholder="Enter Price" 
                        required
                    />
                </Form.Group>

                <Form.Group>
                    <Form.Label>End Date</Form.Label>
                    <Form.Control 
                        type="date"
                        required
                    />
                </Form.Group>


                <Button className="Primary" type="submit">
                   Edit
                </Button>
            </Form>
        </Fragment>
    )
}