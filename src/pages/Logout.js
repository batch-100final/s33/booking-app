// base imports
import React, { useContext, useEffect } from 'react';
import { Redirect } from 'react-router-dom';
import UserContext from '../UserContext';

export default function Logout() {
	const { unsetUser, setUser } = useContext(UserContext);
	// clear the local storage of the user's information
	unsetUser();
	// set the user state back to it's original value
	setUser({email:null});

	// invoke unsetUser only after initial render
	useEffect(() => {
		unsetUser()
	})

	// redirect back to login
	// localStorage.clear();
	return (
		<Redirect to='/login' />
	)
}
