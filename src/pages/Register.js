// base imports
import React, { useState, useEffect } from 'react';

// bootstrap
import { Form, Button, Container } from 'react-bootstrap';

// export default allows the function to be used in other files
export default function Register() {
	const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [passwordConfirm, setPasswordConfirm] = useState('');
    const [isActive, setIsActive] = useState(true);


	function register(e) {
        // prevents page redirection
        e.preventDefault();
        alert("Registration successful. You may now log in.");

        // clear the input fields
        setEmail('');
        setPassword('');
        setPasswordConfirm(''); 
    }

    useEffect(() => {
        let isEmailNotEmpty = email !== '';
        let isPasswordNotEmpty = password !== '';
        let isPasswordConfirmNotEmpty = passwordConfirm !== '';
        let isPasswordMatched = password === passwordConfirm;

        if (isEmailNotEmpty && isPasswordNotEmpty && isPasswordConfirmNotEmpty && isPasswordMatched) {
            setIsActive(false);
        } else {
            setIsActive(true);
        }

    }, [email, password, passwordConfirm]);
    // useEffect(() => {
    // 	console.log(email);
    // }, [email]);

    // useEffect(() => {
    // 	console.log(password);
    // }, [password]);


	return (
		<Container>
			<h3>Register</h3>
			<Form onSubmit={(e) => register(e)}>
				<Form.Group>
					<Form.Label>Email Address</Form.Label>
					<Form.Control 
					type="email" 
					placeholder="Enter email" 
					value={email} 
					onChange={(e) => setEmail(e.target.value)}
					required
				/> 
				</Form.Group> 

				<Form.Group>
					<Form.Label>Password</Form.Label>
					<Form.Control type="password" placeholder="Enter password" value={password} onChange={(e) => setPassword(e.target.value)} required/> 
				</Form.Group>

				<Form.Group>
					<Form.Label>Confirm Password</Form.Label>
					<Form.Control type="password" placeholder="Confirm Passowrd" value={passwordConfirm} onChange={(e) => setPasswordConfirm(e.target.value)} required/> 
				</Form.Group>

				<Button className="bg-primary" type="submit" disabled={isActive}>Submit</Button>

			</Form>
		</Container>
	)
}